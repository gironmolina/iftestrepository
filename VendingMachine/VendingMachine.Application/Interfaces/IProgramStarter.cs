﻿namespace VendingMachine.Application.Interfaces
{
    public interface IProgramStarter
    {
        void StartVendingMachine();
    }
}
