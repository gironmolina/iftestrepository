using VendingMachine.Entities;

namespace VendingMachine.Application.Builders
{
    public class ProductBuilder
    {
        private Product innerObject = new Product();

        public static implicit operator Product(ProductBuilder instance)
        {
            return instance.Build();
        }

        public ProductBuilder Available(int available)
        {
            innerObject.Available = available;
            return this;
        }

        public ProductBuilder Price(Money price)
        {
            innerObject.Price = price;
            return this;
        }

        public ProductBuilder Name(string name)
        {
            innerObject.Name = name;
            return this;
        }

        private Product Build()
        {
            return innerObject;
        }
    }
}