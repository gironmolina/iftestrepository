﻿using System;
using System.Linq;
using VendingMachine.Library.Interfaces;
//using VendingMachine.Library.Model;

namespace VendingMachine.Library
{
    public class VendingMachine : IVendingMachine
    {
        //public Money Balance { get; set; }
        //public string Manufacturer { get; }

        //public Money Amount { get; private set; }

        //public Product[] Products { get; set; }

        //public VendingMachine(Money initialAmount, Product[] products, string manufacturer = "")
        //{
        //    if (products != null)
        //    {
        //        for (var i = 0; i < products.GetLength(0); i++)
        //        {
        //            products[i].Price = CalculateEuros(products[i].Price);
        //        }
        //    }
        //    Manufacturer = manufacturer;
        //    Amount = initialAmount;
        //    Products = products;
        //}

        public VendingMachine()
        {
        }

        //public Money InsertCoin(Money money)
        //{
        //    try
        //    {
        //        Amount = UpdateMoney(money, Amount);
        //        Balance = UpdateMoney(money, Balance);
        //        return Balance;
        //    }
        //    catch (Exception e)
        //    {
        //        throw new VendingMachineException("Error inserting coins", e);
        //    }
        //}

        //public Money ReturnMoney()
        //{
        //    try
        //    {
        //        var returnedMoney = Balance;
        //        Amount = UpdateMoney(Balance, Amount, true);
        //        // Clear Balance
        //        Balance = new Money();
        //        return CalculateEuros(returnedMoney);
        //    }
        //    catch (Exception e)
        //    {
        //        throw new VendingMachineException("Error returning money", e);
        //    }
        //}

        //public Product Buy(int productNumber)
        //{
        //    try
        //    {
        //        var index = productNumber - 1;
        //        var product = Products.ElementAtOrDefault(index);

        //        if (product.Name == null)
        //            return default(Product);

        //        if (IsPositiveBalance(Balance, product.Price))
        //        {
        //            // Update product availables and update balance
        //            product.Available--;
        //            Products[index].Available = product.Available;
        //            Balance = UpdateMoney(product.Price, Balance, true);
        //        }
        //        else
        //            product = default(Product);

        //        return product;
        //    }
        //    catch (Exception e)
        //    {
        //        throw new VendingMachineException("Error buying product", e);
        //    }
        //}

        //public bool UpdateProduct(int productIndex, int available, Money price, string name)
        //{
        //    try
        //    {
        //        var index = productIndex - 1;
        //        var product = Products.ElementAtOrDefault(index).Name;
        //        if (product == null)
        //            return false;

        //        var calculateEuros = CalculateEuros(price);
        //        Products[index] = new Product {Name = name, Available = available, Price = calculateEuros};
        //        return true;
        //    }
        //    catch (Exception e)
        //    {
        //        throw new VendingMachineException("Error updating product", e);
        //    }
        //}

        //public Money UpdateMoney(Money moneyInserted, Money moneyToUpdate, bool isRemovedMoney = false)
        //{
        //    try
        //    {
        //        if (!isRemovedMoney)
        //        {
        //            var totalMoney = new Money
        //            {
        //                Euros = moneyToUpdate.Euros + moneyInserted.Euros,
        //                Cents = moneyToUpdate.Cents + moneyInserted.Cents
        //            };

        //            var moneyConverted = CalculateEuros(totalMoney);

        //            moneyToUpdate.Euros = moneyConverted.Euros;
        //            moneyToUpdate.Cents = moneyConverted.Cents;
        //        }
        //        else
        //        {
        //            var totalMoney = new Money
        //            {
        //                Euros = moneyToUpdate.Euros - moneyInserted.Euros,
        //                Cents = moneyToUpdate.Cents - moneyInserted.Cents
        //            };
        //            var moneyConverted = CalculateEuros(totalMoney, true);

        //            moneyToUpdate.Euros = moneyConverted.Euros;
        //            moneyToUpdate.Cents = moneyConverted.Cents;
        //        }
        //        return moneyToUpdate;
        //    }
        //    catch (Exception e)
        //    {
        //        throw new VendingMachineException("Error updating money", e);
        //    }
        //}

        ///// <summary>
        ///// Convert Cents to Euros
        ///// </summary>
        ///// <param name="moneyToConvert"></param>
        ///// <param name="isRemainderMoney">Indicates if it´s remainder money</param>
        ///// <returns></returns>
        //private Money CalculateEuros(Money moneyToConvert, bool isRemainderMoney = false)
        //{
        //    try
        //    {
        //        if (!isRemainderMoney)
        //        {
        //            var resultMoney = new Money {Euros = moneyToConvert.Euros};
        //            resultMoney.Euros += (int) Math.Floor((double) moneyToConvert.Cents / 100);
        //            resultMoney.Cents += moneyToConvert.Cents % 100;
        //            return resultMoney;
        //        }
        //        else
        //        {
        //            var resultMoney = new Money {Euros = moneyToConvert.Euros};
        //            resultMoney.Euros -= Math.Abs((int) Math.Floor((double) moneyToConvert.Cents / 100));
        //            if (moneyToConvert.Cents >= 0)
        //            {
        //                resultMoney.Cents = moneyToConvert.Cents % 100;
        //            }
        //            else
        //            {
        //                resultMoney.Cents = 100 - Math.Abs(moneyToConvert.Cents % 100);
        //            }
        //            return resultMoney;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        throw new VendingMachineException("Error calculating euros", e);
        //    }
        //}

        ///// <summary>
        ///// Check Existence and Availability
        ///// </summary>
        ///// <param name="productIndex"></param>
        ///// <param name="checkAvailability">Indicates if Check Availability is required</param>
        ///// <returns></returns>
        //public bool CheckExistence(int productIndex, bool checkAvailability = false)
        //{
        //    try
        //    {
        //        var index = productIndex - 1;
        //        var product = Products.ElementAtOrDefault(index);

        //        if (product.Name == null)
        //            return false;

        //        if (!checkAvailability)
        //            return true;

        //        return product.Available > 0;
        //    }
        //    catch (Exception e)
        //    {
        //        throw new VendingMachineException("Error checking existence", e);
        //    }
        //}

        ///// <summary>
        ///// Get Positive money Balance
        ///// </summary>
        ///// <param name="greater">Greater value</param>
        ///// <param name="less">Less value</param>
        ///// <returns></returns>
        //public bool IsPositiveBalance(Money greater, Money less)
        //{
        //    try
        //    {
        //        if (greater.Euros > less.Euros)
        //            return true;

        //        if (greater.Euros < less.Euros)
        //            return false;
            
        //        if (greater.Cents > less.Cents)
        //        {
        //            return true;
        //        }

        //        return greater.Cents >= less.Cents;
        //    }
        //    catch (Exception e)
        //    {
        //        throw new VendingMachineException("Error calculating positive balance", e);
        //    }
        //}
        public string Manufacturer { get; }
    }
}