﻿using NUnit.Framework;
//using VendingMachine.Library.Interfaces;
//using VendingMachine.Library.Model;

namespace VendingMachine.Library.Test
{
    [TestFixture]
    public class VendingMachineTest
    {
        //private Product[] products;

        //[SetUp] 
        //public void SetUp()
        //{
        //    products = new[] {new Product {Available = 3, Name = "Coke", Price = new Money {Euros = 1, Cents = 50}},
        //                      new Product {Available = 2, Name = "Snacks", Price = new Money {Euros = 2, Cents = 20}},
        //                      new Product {Available = 1, Name = "Water", Price = new Money {Euros = 1, Cents = 0}}};
        //}

        //[TestCase(1, 2, 1, 5, "Test1", ExpectedResult = true)]
        //[TestCase(4, 1, 10, 50, "Test1", ExpectedResult = false)]
        //public bool VendingMachine_UpdateProduct(int productNumber, int availableNumber, int euros, int cent, string name)
        //{
        //    //Arrange
        //    VendingMachine vendingMachinePlus = new VendingMachine(new Money(), products);
        //    //Act
        //    var price = new Money { Euros = euros, Cents = cent };
        //    //Assert
        //    return vendingMachinePlus.UpdateProduct(productNumber, availableNumber, price, name);
        //}

        //[Test]
        //public void VendingMachine_InsertCoin(
        //    [Values(1, 1, 0)] int amountEuros,
        //    [Values(50, 0, 10)] int amountCents,
        //    [Values(0, 1, 0)] int balanceEuros,
        //    [Values(160, 20, 50)] int balanceCents
        //    )
        //{
        //    //Arrange
        //    var vendingMachine = new VendingMachine(new Money { Euros = amountEuros, Cents = amountCents }, products)
        //    {
        //        Balance = new Money {Euros = amountEuros, Cents = amountCents }
        //    };
        //    var moneyInserted = new Money { Euros = balanceEuros, Cents = balanceCents };
        //    var previousBalance = vendingMachine.Balance;

        //    //Act
        //    var actual = vendingMachine.InsertCoin(moneyInserted);
        //    var condition = actual.Euros > previousBalance.Euros || actual.Cents > previousBalance.Cents;

        //    //Assert
        //    Assert.IsTrue(condition);
        //}

        //[Test]
        //public void VendingMachine_UpdateMoney(
        //    [Values(1, 3)] int euros,
        //    [Values(2, 4)] int eurosToUpdate,
        //    [Values(10, 20)] int cents,
        //    [Values(5, 50)] int centsToUpdate
        //    )
        //{
        //    //Arrange
        //    VendingMachine vendingMachinePlus = new VendingMachine(new Money(), null);
        //    var money = new Money { Euros = euros, Cents = cents };
        //    var moneyToUpdate = new Money { Euros = eurosToUpdate, Cents = centsToUpdate};
        //    //Act
        //    moneyToUpdate = vendingMachinePlus.UpdateMoney(money, moneyToUpdate);
        //    var result = new Money { Euros = euros + eurosToUpdate, Cents = cents + centsToUpdate};

        //    //Assert
        //    Assert.AreEqual(result.Euros, moneyToUpdate.Euros);
        //    Assert.AreEqual(result.Cents, moneyToUpdate.Cents);
        //}

        //[Test]
        //public void VendingMachine_ReturnMoney(
        //    [Values(5, 1)] int initialEurosAmount,
        //    [Values(20, 55)] int initialCentsAmount,
        //    [Values(1, 0)] int balanceEuros,
        //    [Values(130, 5)] int balanceCents
        //    )
        //{
        //    //Arrange
        //    var vendingMachine = new VendingMachine(new Money {Euros = initialEurosAmount, Cents = initialCentsAmount}, null)
        //    {
        //        Balance = new Money {Euros = balanceEuros, Cents = balanceCents}
        //    };
        //    //Act
        //    var previousAmount = vendingMachine.Amount;
        //    vendingMachine.ReturnMoney();
        //    var condition = vendingMachine.Amount.Euros <= previousAmount.Euros || vendingMachine.Amount.Cents <= previousAmount.Cents;
            
        //    //Assert
        //    Assert.IsTrue(condition);
        //}

        //[Test]
        //public void VendingMachine_IsPositiveBalance_True(
        //    [Values(1)] int balanceEuros,
        //    [Values(5)] int balanceCents,
        //    [Values(1)] int priceEuros,
        //    [Values(5,4)] int priceCents
        //    )
        //{
        //    //Arrange
        //    var vendingMachine = new VendingMachine(new Money(), null);

        //    //Act
        //    var result = vendingMachine.IsPositiveBalance(
        //        new Money {Euros = balanceEuros, Cents = balanceCents}, 
        //        new Money {Euros = priceEuros, Cents = priceCents });

        //    //Assert
        //    Assert.IsTrue(result);
        //}

        //[Test]
        //public void VendingMachine_BuyProduct_Successfully(
        //    [Values(10)] int initialEurosAmount,
        //    [Values(20)] int initialCentsAmount,
        //    [Values(3)] int balanceEuros,
        //    [Values(50)] int balanceCents,
        //    [Values(1,2,3)] int productIndex
        //    )
        //{
        //    //Arrange
        //    IVendingMachine vendingMachine = new VendingMachine(new Money { Euros = initialEurosAmount, Cents = initialCentsAmount }, products)
        //    {
        //        Balance = new Money { Euros = balanceEuros, Cents = balanceCents }
        //    };

        //    //Act
        //    var actual = vendingMachine.Products[productIndex - 1];
        //    var product = vendingMachine.Buy(productIndex);

        //    //Assert
        //    Assert.Less(product.Available, actual.Available);
        //}

        //[Test]
        //public void VendingMachine_CheckExistence_True(
        //    [Values(1,2,3)] int productIndex
        //    )
        //{
        //    //Arrange
        //    var vendingMachine = new VendingMachine(new Money(), products);

        //    //Act
        //    var actual = vendingMachine.CheckExistence(productIndex);

        //    //Assert
        //    Assert.IsTrue(actual);
        //}

        //[Test]
        //public void VendingMachine_CheckAvailability_True(
        //    [Values(1, 2, 3)] int productIndex
        //    )
        //{
        //    //Arrange
        //    var vendingMachine = new VendingMachine(new Money(), products);

        //    //Act
        //    var actual = vendingMachine.CheckExistence(productIndex, true);

        //    //Assert
        //    Assert.IsTrue(actual);
        //}
    }
}