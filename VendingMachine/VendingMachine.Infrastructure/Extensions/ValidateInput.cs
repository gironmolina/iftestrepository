﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachine.Infrastructure.Extensions
{
    public static class ValidateInput
    {
        public static bool IsInteger(this string str)
        {
            return !string.IsNullOrEmpty(str) && int.TryParse(str, out _);
        }

        public static bool IsBetweenRange(this int value, int min, int max)
        {
            return value >= min && value <= max;
        }
    }
}
