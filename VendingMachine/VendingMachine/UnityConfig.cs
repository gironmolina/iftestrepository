﻿using System;
using System.Collections.Generic;
using Unity;
using Unity.Injection;
using VendingMachine.Application.Builders;
using VendingMachine.Application.Interfaces;
using VendingMachine.Entities;

namespace VendingMachine
{
    public static class UnityConfig
    {
        private static readonly List<Product> Products = new List<Product>
        {
            new ProductBuilder().Available(3).Name("Coke").Price(new Money {Euros = 1, Cents = 50}),
            new ProductBuilder().Available(0).Name("Snacks").Price(new Money {Euros = 2, Cents = 20}),
            new ProductBuilder().Available(1).Name("Water").Price(new Money {Euros = 1, Cents = 0})
        };

        private static readonly Lazy<IUnityContainer> UnityContainer = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        });

        /// <summary>
        /// Configured Unity LoadContainers.
        /// </summary>
        public static IUnityContainer LoadContainers => UnityContainer.Value;

        public static void RegisterTypes(IUnityContainer container)
        {
            container.RegisterType<IVendingMachine, Application.VendingMachine>(new InjectionConstructor(new Money { Euros = 100 }, Products, "If..."));
        }
    }
}