﻿using System;
using Unity;
using VendingMachine.Application;

namespace VendingMachine
{
    internal class Program
    {
        private static IUnityContainer container;
        
        private static void Main()
        {
            container = UnityConfig.LoadContainers;
            var programStarter = container.Resolve<ProgramStarter>();
            programStarter.StartVendingMachine();
            Console.ReadLine();
        }
    }
}