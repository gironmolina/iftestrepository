﻿namespace VendingMachine.Entities
{
    public struct Money
    {
        public int Euros { get; set; }
        public int Cents { get; set; }
    }
}
