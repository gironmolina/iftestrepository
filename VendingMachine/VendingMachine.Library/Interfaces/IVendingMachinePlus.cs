﻿using VendingMachine.Entities;

namespace VendingMachine.Library.Interfaces
{
    public interface IVendingMachinePlus
    {
        /// <summary>User money balance.</summary>
        IMoney Balance { get; set; }

        /// <summary>Updates vending machine products.</summary>
        /// <param name="productNumber">Product number.</param>
        /// <param name="available">Available amount of product.</param>
        /// <param name="price">Product price.</param>
        /// <param name="name">Product name</param>
        bool UpdateProduct(int productNumber, int available, IMoney price, string name);
        
        /// <summary>Updates money of the vending machine</summary>
        /// <param name="money">Coin amount.</param>
        /// <param name="moneyToUpdate">Money to update.</param>
        /// <param name="isRemovedMoney">Indicates if is to remove money</param>
        /// <returns></returns>
        IMoney UpdateMoney(IMoney money, IMoney moneyToUpdate, bool isRemovedMoney = false);
    }
}
