﻿using System;
using System.Collections.Generic;
using Unity.Attributes;
using VendingMachine.Entities;
using VendingMachine.Infrastructure.Consts;
using VendingMachine.Library.Interfaces;

namespace VendingMachine.Library
{
    public class ProgramStarter : IProgramStarter
    {
        private const string INVALID_OPTION = "Invalid Value";

        private readonly List<Product> products = new List<Product>{
            new Product {Available = 3, Name = "Coke", Price = new Money {Euros = 1, Cents = 500}},
            new Product {Available = 0, Name = "Snacks", Price = new Money {Euros = 2, Cents = 20}},
            new Product {Available = 1, Name = "Water", Price = new Money {Euros = 1, Cents = 0}}};

        [Dependency]
        public IVendingMachine VendingMachineService { get; set; }

        private readonly IVendingMachine vendindMachineService;

        public ProgramStarter()
        {
            // Unity has created this instance and resolved all dependencies.
        }

        public void StartVendingMachine()
        {
            try
            {
                vendingMachine = new Library.VendingMachine(new Money { Euros = 100 }, Products, "If...");

                while (true)
                {
                    Console.WriteLine("--------------------------------------------------------");
                    Console.WriteLine($"Your current money balance is {vendingMachine.Balance.Euros} Euros {vendingMachine.Balance.Cents} Cents");
                    Console.WriteLine("Please, select an option to execute:");
                    Console.WriteLine("1 - Update Product List");
                    Console.WriteLine("2 - Insert coins");
                    Console.WriteLine("3 - Return money");
                    Console.WriteLine("4 - Buy a product");
                    Console.WriteLine("5 - Exit");
                    int selectedOption;
                    do
                    {
                        selectedOption = Convert.ToInt32(ValidateField(true));

                        if (selectedOption < 1 || selectedOption > 5)
                            Console.WriteLine(SystemMessages.);
                        else
                            break;
                    } while (true);

                    ExecuteSelectedOption(selectedOption);
                }
            }
            catch (VendingMachineException e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e);
                Console.WriteLine("I am sorry, there was a Vending Machine Exception");
                Console.ReadKey();
            }
            catch (IndexOutOfRangeException e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e);
                Console.WriteLine("I am sorry, there was an Index Out of Range Exception");
                Console.ReadKey();
            }
            catch (NullReferenceException e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e);
                Console.WriteLine("I am sorry, there was a Null Reference Exception");
                Console.ReadKey();
            }
            catch (OverflowException e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e);
                Console.WriteLine("I am sorry, there was an Overflow Exception");
                Console.ReadKey();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e);
                Console.WriteLine("I am sorry, there was a General Exception");
                Console.ReadKey();
            }
        }

        private static void ExecuteSelectedOption(int selectedOption)
        {
            switch (selectedOption)
            {
                case 1:
                    UpdateOption();
                    break;

                case 2:
                    InsertCoinsOption();
                    break;

                case 3:
                    ReturnMoneyOption();
                    break;

                case 4:
                    BuyOption();
                    break;

                case 5:
                    Environment.Exit(0);
                    break;
            }
        }

        private static void UpdateOption()
        {
            var result = false;
            int productIndex;
            while (true)
            {
                Console.WriteLine("Please enter a valid product index to edit");
                productIndex = Convert.ToInt32(ValidateField(true));
                if (vendingMachine.CheckExistence(productIndex))
                    break;
                Console.WriteLine(INVALID_OPTION);
            }

            Console.WriteLine("Please enter product name");
            var productName = ValidateField();

            Console.WriteLine("Please enter price (Euros)");
            var priceEuros = Convert.ToInt32(ValidateField(true));

            Console.WriteLine("Please enter price (Cents)");
            var priceCents = Convert.ToInt32(ValidateField(true));

            Console.WriteLine("Please enter products available");
            var productAvailable = Convert.ToInt32(ValidateField(true));

            var vendingMachinePlus = vendingMachine as IVendingMachinePlus;
            if (vendingMachinePlus != null)
                result = vendingMachinePlus.UpdateProduct(productIndex, productAvailable, new Money { Euros = priceEuros, Cents = priceCents }, productName);

            if (result)
            {
                Console.WriteLine("Product Updated Successfully!");
            }
        }

        private static void InsertCoinsOption()
        {
            Console.WriteLine("Select a coin to insert:");
            Console.WriteLine("1 - 1 Euro");
            Console.WriteLine("2 - 2 Euros");
            Console.WriteLine("3 - 5 Cents");
            Console.WriteLine("4 - 10 Cents");
            Console.WriteLine("5 - 20 Cents");
            Console.WriteLine("6 - 50 Cents");

            int selectedOption;
            do
            {
                selectedOption = Convert.ToInt32(ValidateField(true));

                if (selectedOption < 1 || selectedOption > 6)
                    Console.WriteLine(INVALID_OPTION);
                else
                    break;
            } while (true);

            var insertedCoin = new Money();
            switch (selectedOption)
            {
                case 1:
                    insertedCoin = new Money { Euros = 1 };
                    break;
                case 2:
                    insertedCoin = new Money { Euros = 2 };
                    break;
                case 3:
                    insertedCoin = new Money { Cents = 5 };
                    break;
                case 4:
                    insertedCoin = new Money { Cents = 10 };
                    break;
                case 5:
                    insertedCoin = new Money { Cents = 20 };
                    break;
                case 6:
                    insertedCoin = new Money { Cents = 50 };
                    break;
            }
            vendingMachine.InsertCoin(insertedCoin);
        }

        private static void ReturnMoneyOption()
        {
            var moneyReturned = vendingMachine.ReturnMoney();
            Console.WriteLine($"It were returned {moneyReturned.Euros} Euros {moneyReturned.Cents} Cents");
        }

        private static void BuyOption()
        {
            Console.WriteLine("--------------------------------------------------------");
            Console.WriteLine("Please, select a product:");
            var i = 1;
            foreach (var product in vendingMachine.Products)
            {
                Console.WriteLine($"#{i++} - Available: {product.Available} - Name: {product.Name} - Price: {product.Price.Euros},{product.Price.Cents} Euros");
            }

            var productSelected = Convert.ToInt32(ValidateField(true));
            var isAvailable = vendingMachine.CheckExistence(productSelected, true);
            if (!isAvailable)
            {
                Console.WriteLine("Product not available");
                return;
            }

            var result = vendingMachine.Buy(productSelected);
            Console.WriteLine(result.Name != null
                ? $"Thanks for your purchase, Enjoy your {result.Name}"
                : "Sorry, Insufficient balance");
        }

        private static string ValidateField(bool isInteger = false)
        {
            string fieldValue;
            while (true)
            {
                fieldValue = Console.ReadLine();
                if (string.IsNullOrEmpty(fieldValue))
                {
                    Console.WriteLine(INVALID_OPTION);
                    continue;
                }

                if (!isInteger)
                    break;

                int selectedOption;
                while (!(int.TryParse(fieldValue, out selectedOption) && selectedOption >= 0))
                {
                    Console.WriteLine(INVALID_OPTION);
                    fieldValue = Console.ReadLine();
                }
                break;
            }
            return fieldValue;
        }
    }
}
