﻿namespace VendingMachine.Library.Interfaces
{
    public interface IProgramStarter
    {
        void StartVendingMachine();
    }
}
