# Vending Machine Test #

This code emulates a Vending Machine using C# and Console Application. There are also some unit testing available for NUnit.

![Alt text](https://www.vending.com/files/2015/06/Used-40-Select-SnackCandy.png)

### Reatures ###

* Update product list.
* Insert coins, get coins back and get remainder.
* Buy products.
* Convert cents to euros.

### What's Inside ###

* [TDD](https://en.wikipedia.org/wiki/Test-driven_development)
* [NUnit](https://www.nunit.org)

### Prerequisites ###

* [NUnit](https://www.nuget.org/packages/NUnit) 2.6.4
* [NUnitTestAdapter](https://www.nuget.org/packages/NUnitTestAdapter.WithFramework) 2.0

### Who do I talk to? ###

* Huans Hirons