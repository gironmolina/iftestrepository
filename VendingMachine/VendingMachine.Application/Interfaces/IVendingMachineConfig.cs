﻿using VendingMachine.Entities;

namespace VendingMachine.Application.Interfaces
{
    public interface IVendingMachineConfig
    {
        /// <summary>User money balance.</summary>
        Money Balance { get; set; }

        /// <summary>Updates vending machine products.</summary>
        /// <param name="productNumber">Product number.</param>
        /// <param name="available">Available amount of product.</param>
        /// <param name="price">Product price.</param>
        /// <param name="name">Product name</param>
        bool UpdateProduct(int productNumber, int available, Money price, string name);
        
        /// <summary>Updates money of the vending machine</summary>
        /// <param name="money">Coin amount.</param>
        /// <param name="moneyToUpdate">Money to update.</param>
        /// <param name="isRemovedMoney">Indicates if is to remove money</param>
        /// <returns></returns>
        Money UpdateMoney(Money money, Money moneyToUpdate, bool isRemovedMoney = false);
    }
}
